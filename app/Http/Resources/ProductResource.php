<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class ProductResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
      return[
        'name'=>$this->name,
        'actual_price'=>'Rs '.$this->price,
        'discounted_price'=>'Rs '.((1-$this->discount/100)*$this->price),
        'stock'=>$this->isstock($this->stock),
         'href'=>[
           'link'=>route('reviews.index',$this->id)
         ]
      ];
    }
    public function isstock($vstock){
      if($vstock>0)
      {
        return $vstock;
      }
      else{
        return "out of stock";
      }
    }
}
