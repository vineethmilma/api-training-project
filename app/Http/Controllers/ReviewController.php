<?php

namespace App\Http\Controllers;

use App\Review;
use Illuminate\Http\Request;

class ReviewController extends Controller
{
  // public function __construct()
  // {
  //   $this->middleware('auth:api');
  // }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($product)
    {
        return (Review::where('product_id','=',$product)->get());
    }



    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,$product)
    {
        $review=Review::create($request->all()+['product_id'=>$product]);
        return $review;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Review  $review
     * @return \Illuminate\Http\Response
     */

    public function show($product,$review)
    {
      return (Review::where('product_id',$product)->where('id',$review)->get());
    }



    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Review  $review
     * @return \Illuminate\Http\Response
     */
    public function update($id,Request $request,Review $review)
    {
       $review->update($request->all()+['product_id'=>$id]);
        return($review);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Review  $review
     * @return \Illuminate\Http\Response
     */
    public function destroy($id,Review $review)
    {
      $review->delete();
      return('data deleted');
    }


    


}
