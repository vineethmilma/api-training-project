<?php

use Faker\Generator as Faker;
use App\Product;

$factory->define(App\Review::class, function (Faker $faker) {
    return
     [
        'product_id'=> function(){
          return Product::all()->random();
        },
        'review'=>$faker->realText(100),
        'customer'=>$faker->name,
        'star'=>$faker->numberBetween(1,5),

    ];
});
